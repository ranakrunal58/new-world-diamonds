(function(){
  var useBoltConfirmationPage = false
  var orderNotesId = ""
  var enableGaTracking = true
  var enableFacebookPixel = true;
  var noShopifyConfirmationSlicing = true;
  var freeShippingVal = 0
  var defaultShippingOption = {
    service: "",
    cost: {
      amount:0,
      currency:"USD",
      currency_symbol:"$"
    }
  }



  // DO NOT MODIFY BEYOND THIS POINT (UNLESS SPECIALLY REFERENCED MODIFICATION)
  var orderNote = ""
  var cartUpdateURLs = [
    '/cart/change.js',
    '/cart/update.js',
    '/cart/add.js',
  ];
  var trackGaStep = function(step) {
    if (!enableGaTracking) {
      return;
    }
    ga('ec:setAction','checkout', {
      'step': step,
    });
    ga('send', 'event', 'Checkout', 'step');
  };

  function loadCart() {
    if (typeof BoltCheckout === "undefined") {
      return;
    }
    if (typeof $ !== "undefined" && $(".bolt-checkout-button") && $(".bolt-checkout-button").length === 0) {
      // Not a cart page.
      return;
    }
    var hints = {};
    var tax_exempt = false;

    var Shopifyhints = {};
  	var discountShippingParams = {};
    var callbacks ={
        check: function() {
          trackGaStep(1);
          return true;
        },
        onCheckoutStart: function() {
          trackGaStep(2);
          if (enableFacebookPixel) {
            fbq('track', 'InitiateCheckout');
          }
        },
        onShippingDetailsComplete: function() {
            trackGaStep(3);
        },
        onShippingOptionsComplete: function() {
             trackGaStep(4);
        },
        onPaymentSubmit: function() {
            trackGaStep(5);
          if (enableFacebookPixel) {
              fbq('track', 'AddPaymentInfo');
          }
         },
        success: function(transaction, callback) {
          reference = transaction.reference;
          if (enableGaTracking) {
            ga('ec:setAction', 'purchase', {
              'id': transaction.reference,
              'revenue': String(transaction.amount.amount / 100.0)
            });
            ga('send', 'event');

            trackGaStep(6);
          }
          if (enableFacebookPixel) {
            fbq('track', 'Purchase', {
              value: transaction.amount.amount / 100,
              currency: 'USD',
            });
          }
        }
    };
    Shopifyhints["useBoltOrderConfirmationPage"] = useBoltConfirmationPage;
		Shopifyhints["noShopifyConfirmationSlicing"] = noShopifyConfirmationSlicing;
    if (freeShippingVal != 0) {
      Shopifyhints["freeShippingCentsValue"] = freeShippingVal;
      if (typeof defaultShippingOption !== 'undefined') {
        Shopifyhints["defaultShippingOption"] = defaultShippingOption
      }
    }
    if (orderNote !== "") {
      Shopifyhints["orderNotesMessage"] = orderNote;
    }
    Shopifyhints["taxExempt"] = tax_exempt;
    BoltCheckout.configureShopify(Shopifyhints, hints, callbacks);
  }
  $(document).ready(function(){
    $(document).ajaxComplete(function(event, xhr, settings) {
      for (var i = 0; i < cartUpdateURLs.length; i++) {
        if (settings && settings.url.includes(cartUpdateURLs[i]) && (!settings.data || !(settings.data.includes("bolt=true")))) {
          loadCart();
        }
      }
    });
    loadCart();
    window.boltLoadCart = loadCart;
    $(".bolt-checkout-button").css("width", "auto");
    $(document).on('DOMNodeInserted', function(e) {
      if ($(e.target).hasClass('bolt-checkout-button-button')) {
        $(e.target).css("margin-right", "0px");
        $(e.target).css("width", "100%");
      }
      if ($(e.target).hasClass('bolt-checkout-button-cards')) {
        $(e.target).css("float", "none");
        $(e.target).css("width", "auto");
      }
    });

    if (orderNotesId != "") {
      $("#" + orderNotesId).on('input', function() {
        orderNote = $("#" + orderNotesId).val();
        loadCart();
      });
    }
  });
})();

if (typeof BoltCheckout !== "undefined") {
  BoltCheckout.configure();
}
